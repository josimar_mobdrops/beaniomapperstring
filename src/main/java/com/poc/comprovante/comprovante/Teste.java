package com.poc.comprovante.comprovante;

import lombok.extern.java.Log;
import org.beanio.StreamFactory;
import org.beanio.Unmarshaller;
import org.beanio.builder.FixedLengthParserBuilder;
import org.beanio.builder.StreamBuilder;

@Log
public class Teste {

    
    private String teste;
    
    public static void main(String[] args) {

        String is2b = "PROTOCOLO 0003025332\\r\\n1          19/09/2019        10:23\\r\\nTERM 228005 AGENTE 228005 AUTE 01567\\r\\n----------------------------------------\\r\\nAUTO 052109           RECEBIMENTO CONTA\\r\\n                    \\r\\n       CONTA - SEGMENTO TELEFONE\\r\\n      84630000002-9 61220072001-1      \\r\\n         10329914956-9 08190190826-9\\r\\n----------------------------------------\\r\\nVALOR COBRADO                    261,22\\r\\n\\r\\n    VALIDO COMO RECIBO DE PAGAMENTO\\r\\n----------------------------------------\\r\\n              AUTENTICACAO\\r\\n        CE.61.52.45.82.FF.8C.69\\r\\n        AF.5C.20.08.68.C6.3D.CA\\r\\n----------------------------------------\\r\\n\\r\\n";
        StreamFactory factory = StreamFactory.newInstance();
        factory.define(new StreamBuilder("inputVariables")
                .format("fixedlength")
                .parser(new FixedLengthParserBuilder())
                .addRecord(ReciboComprovante.class));
        Unmarshaller unmarshaller = factory.createUnmarshaller("inputVariables");

        ReciboComprovante o = (ReciboComprovante) unmarshaller.unmarshal(is2b);

        log.info(o.toString());

    }
}
