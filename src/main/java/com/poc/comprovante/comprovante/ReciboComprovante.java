package com.poc.comprovante.comprovante;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beanio.annotation.Field;
import org.beanio.annotation.Fields;
import org.beanio.annotation.Record;

@NoArgsConstructor
@Getter
@Setter
@Record(name = "PROTOCOLO")
@Fields({
        @Field(at=10, name = "recordType", rid = true, literal = "PROTOCOLO")
})
public class ReciboComprovante {

    @Field(at = 0, length = 8)
    private String teste;

    @Field(at = 9, length = 10)
    private String protocolo;

    @Field(at = 24, length = 10)
    private String dataPagamento;

    @Field(at = 34, length = 5)
    private String horaPagamento;


    @Field(at = 47, length = 6)
    private String term;

    @Field(at = 59, length = 6)
    private String agente;

    @Field(at = 69, length = 5)
    private String aute;

    @Field(at = 126, length = 6)
    private String auto;

//    @Field(at = )
//    private String recebimentoConta;
//    private String codigoBarra;
//    private String valorCobrado;
//    private String validoReciboPagamento;


}
